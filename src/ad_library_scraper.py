import argparse
import json
import random
import sys
import time
import warnings

import settings
from scrapers.middlewares import ScraperMiddleware
from scrapers.pipelines import WebServicePipeline
from scrapers.scheduler import Scheduler
from utils.io import DataIO

class FacebookAdLibraryScraper:
    """
    A class representing the Facebook Ad Library Scraper.

    Attributes:
        scraper_arguments (dict): Command-line arguments and settings for the scraper.
    """

    def __init__(self, scraper_arguments):
        """
        Initialize the FacebookAdLibraryScraper.

        Args:
            scraper_arguments (dict): Command-line arguments and settings for the scraper.
        """
        self.scraper_arguments = scraper_arguments

    def run(self):
        try:
            # Create an instance of the Scheduler class to manage URL scheduling
            url_scheduler = Scheduler(self.scraper_arguments)

            # Create an instance of the ScraperMiddleware class to process scraped data
            scraper_middleware = ScraperMiddleware(self.scraper_arguments)

            # Create an instance of the WebServicePipeline class to push data to a web service
            web_service_pipeline = WebServicePipeline(self.scraper_arguments)

            # Fetch a list of Facebook page URLs to scrape
            fb_page_urls = url_scheduler.initialize_page_urls()

            # Iterate through each URL and scrape data
            for i, fb_page_url in enumerate(fb_page_urls):
                print(f'Scraping Facebook Page {i + 1}: {fb_page_url}')

                # Process the scraper output for the current page URL
                ads_data = scraper_middleware.process_scraper_output(fb_page_url)

                # Write scraped data to the log file
                DataIO().write_to_log(json.dumps(ads_data, indent=4))

                # Push the scraped data to a web service
                web_service_pipeline.push_to_web_service(ads_data)

                # Add a random sleep interval before processing the next URL
                time.sleep(random.randint(15, 25))

        except Exception as e:
            # Handle exceptions and log errors
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)


if __name__ == '__main__':
    # Disable warnings
    warnings.filterwarnings('ignore')

    # Parse command-line arguments
    argument_parser = argparse.ArgumentParser()
    scraper_arguments = argument_parser.parse_args()

    while True:
        try:
            current_time = DataIO().get_current_time()
            # Check if it's midnight (00:00:00)
            if current_time.hour == 0 and current_time.minute == 0 and current_time.second == 0:
                setattr(scraper_arguments, 'current_time', current_time)
                # Initialize and run the Facebook Ad Library Scraper
                ad_library_scraper = FacebookAdLibraryScraper(scraper_arguments)
                ad_library_scraper.run()
        except Exception as e:
            # Log any errors that occur
            error_message = f'An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        # Sleep to repeat the scraping process at a specified interval
        time.sleep(settings.REPEAT_INTERVAL)
