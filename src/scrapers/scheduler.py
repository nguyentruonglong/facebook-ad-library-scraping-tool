import random
import sys
import requests
import settings
from utils.io import DataIO

class Scheduler:
    """
    Scheduler for initializing Facebook page URLs.

    This class provides a method for initializing a list of Facebook page URLs for scraping.
    """

    def __init__(self, scraper_arguments):
        """
        Initialize the Scheduler.

        Args:
            scraper_arguments: Arguments for the scraper.

        Returns:
            None
        """
        self.scraper_arguments = scraper_arguments

    def initialize_page_urls(self):
        """
        Initialize a generator for Facebook page URLs.

        Yields:
            str: A Facebook page URL.
        """
        try:
            fb_page_urls = list()
            api_fb_page_url = settings.HOSTNAME + settings.API_FACEBOOK_PAGE_PATH

            # Send an HTTP GET request to fetch JSON data
            http_response = requests.get(api_fb_page_url, verify=False, timeout=settings.TIMEOUT)
            json_response = http_response.json()

            # Filter and extract valid page URLs from the JSON response
            fb_page_urls = [entry['page_url'] for entry in json_response if entry.get('page_url')]

            # Shuffle the list of page URLs for randomness
            random.shuffle(fb_page_urls)

            # Yield each page URL
            for fb_page_url in fb_page_urls:
                yield fb_page_url

        except Exception as e:
            # Handle exceptions and log error details
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
            # Yield None to handle the error case
            yield None
