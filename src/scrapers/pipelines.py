import json
import sys
import pytz
import requests
import settings
import queue

from datetime import datetime, timedelta
from threading import Thread
from pymongo import MongoClient
from utils.io import DataIO


class WebServicePipeline:
    """
    Pipeline for sending data to a web service.

    This class provides methods for sending data to a web service using HTTP POST requests.
    """

    def __init__(self, scraper_arguments):
        """
        Initialize the WebServicePipeline with scraper arguments.

        Args:
            scraper_arguments (dict): Arguments for the scraper.

        Returns:
            None
        """
        self.scraper_arguments = scraper_arguments

    def send_post_request(self, url, request_data):
        """
        Send an HTTP POST request to a given URL with JSON data.

        Args:
            url (str): The URL to send the request to.
            request_data (dict): The JSON data to send in the request.

        Returns:
            None
        """
        try:
            # Define headers for the HTTP request
            request_headers = {'content-type': 'application/json'}
            
            # Send the HTTP POST request
            response = requests.post(
                url, data=json.dumps(request_data), headers=request_headers, verify=False, timeout=settings.TIMEOUT)
            
            # Log the HTTP response status code
            error_message = f'HTTP response status code for request to {request_data.get("facebook_page_url", "")}: {response.status_code}'
            DataIO().write_to_log(error_message)
        except Exception as e:
            # Handle exceptions and log the error
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

    def push_to_web_service(self, request_data):
        """
        Push data to a web service in a separate thread.

        Args:
            request_data (dict): The JSON data to push to the web service.

        Returns:
            None
        """
        try:
            # Define the URL for the web service
            api_fbad_url = settings.HOSTNAME + settings.API_FACEBOOK_AD_PATH
            
            # Start a new thread to send the POST request
            Thread(target=self.send_post_request,
                   args=[api_fbad_url, request_data]).start()
        except Exception as e:
            # Handle exceptions and log the error
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)


class ProductInsertionThread(Thread):
    """
    Thread for inserting product items into a MongoDB collection.

    This class is used to insert product items into a MongoDB collection in a separate thread.
    """

    def __init__(self, db_connection, collection, items_to_insert):
        """
        Initialize the ProductInsertionThread.

        Args:
            db_connection: The MongoDB database connection.
            collection: The MongoDB collection.
            items_to_insert: The product items to insert.

        Returns:
            None
        """
        self.db_connection = db_connection
        self.collection = collection
        self.items_to_insert = items_to_insert
        
        # Initialize the thread
        Thread.__init__(self)

    def run(self):
        """
        Run the thread to insert product items into the MongoDB collection.

        Returns:
            None
        """
        try:
            # Insert the product items into the MongoDB collection
            self.collection.insert_many(self.items_to_insert, ordered=False)
        except Exception as e:
            # Handle exceptions and log the error
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)


class ProductQueryThread(Thread):
    """
    Thread for querying product items from a MongoDB collection.

    This class is used to query product items from a MongoDB collection in a separate thread.
    """

    def __init__(self, db_connection, collection, target_product_url, result_queue):
        """
        Initialize the ProductQueryThread.

        Args:
            db_connection: The MongoDB database connection.
            collection: The MongoDB collection.
            target_product_url: The URL of the product to query.
            result_queue: The queue to store the query result.

        Returns:
            None
        """
        self.db_connection = db_connection
        self.collection = collection
        self.target_product_url = target_product_url
        self.result_queue = result_queue
        
        # Initialize the thread
        Thread.__init__(self)

    def run(self):
        """
        Run the thread to query product items from the MongoDB collection.

        Returns:
            None
        """
        try:
            # Get the current time
            current_time = DataIO().get_current_time()
            
            # Calculate the beginning of the current time period
            beginning_time = current_time - timedelta(
                hours=current_time.hour, minutes=current_time.minute,
                seconds=current_time.second, microseconds=current_time.microsecond)
            
            # Convert the beginning_time to UTC timezone
            beginning_time = beginning_time.astimezone(pytz.timezone('UTC'))

            # Delete documents older than the beginning_time
            self.collection.delete_many(
                {'crawled_at': {'$lt': beginning_time}})
            
            # Query a product item based on the target_product_url and timestamp
            queried_product = self.collection.find_one(
                {'product_url': {'$regex': f'.*{self.target_product_url}$'},
                 'crawled_at': {'$gte': beginning_time}},
                {'_id': 0, 'store_url': 0, 'store_status': 0, 'store_id': 0, 'product_id': 0, 'product_url': 0,
                 'product_title': 0, 'crawled_at': 0})

            # If a product is found, convert timestamps to the desired timezone
            if queried_product is not None:
                queried_product['created_at'] = queried_product['created_at'].replace(
                    tzinfo=pytz.UTC)
                queried_product['created_at'] = queried_product['created_at'].astimezone(
                    pytz.timezone(settings.TIMEZONE)).strftime('%Y-%m-%d %H:%M:%S.%f')
                queried_product['updated_at'] = queried_product['updated_at'].replace(
                    tzinfo=pytz.UTC)
                queried_product['updated_at'] = queried_product['updated_at'].astimezone(
                    pytz.timezone(settings.TIMEZONE)).strftime('%Y-%m-%d %H:%M:%S.%f')

            # Put the query result in the result_queue
            self.result_queue.put(queried_product)
        except Exception as e:
            # Handle exceptions and print error details
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)


class MongoDBPipeline:
    """
    Pipeline for interacting with MongoDB.

    This class provides methods for inserting and querying product items from a MongoDB collection.
    """

    def __init__(self):
        # Establish a connection to the MongoDB server with specified settings
        connection = MongoClient(
            host=settings.MONGODB_SERVER,
            port=settings.MONGODB_PORT,
            maxPoolSize=1000,
            waitQueueTimeoutMS=1000,
            waitQueueMultiple=1000
        )

        # Access the specified MongoDB database and collection
        self.db_connection = connection[settings.MONGODB_DB]
        self.collection = self.db_connection[settings.MONGODB_COLLECTION]

    def insert_product_items(self, items):
        """
        Insert product items into the MongoDB collection.

        Args:
            items: The product items to insert.

        Returns:
            None
        """
        try:
            # Create an instance of the ProductInsertionThread to handle the insertion
            insertion_thread = ProductInsertionThread(
                self.db_connection, self.collection, items)
            
            # Set the thread as a daemon
            insertion_thread.setDaemon(True)
            
            # Start the thread
            insertion_thread.start()
            
            # Wait for the thread to complete
            insertion_thread.join()
        except Exception as e:
            # Handle exceptions and log error details
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

    def query_product_item(self, product_url):
        """
        Query a product item from the MongoDB collection.

        Args:
            product_url: The URL of the product to query.

        Returns:
            The queried product item.
        """
        try:
            # Create a result queue to store the query result
            result_queue = queue.Queue()
            
            # Create an instance of the ProductQueryThread to handle the query
            query_thread = ProductQueryThread(
                self.db_connection, self.collection, product_url, result_queue)
            
            # Set the thread as a daemon
            query_thread.setDaemon(True)
            
            # Start the thread
            query_thread.start()
            
            # Wait for the thread to complete
            query_thread.join()
            
            # Get the queried product item from the result queue
            queried_product_item = result_queue.get()
        except Exception as e:
            # Handle exceptions and set queried_product_item to None
            queried_product_item = None
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the queried product item
            return queried_product_item
