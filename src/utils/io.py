import json
import sys
from datetime import datetime
import pytz
import settings
from fake_useragent import UserAgent

class DataIO:
    """
    This class provides functionality related to data input and output operations.
    """

    def get_current_time(self):
        """
        Get the current time with timezone information.

        Returns:
            datetime: Current time with timezone information.
        """
        # Get the current time in UTC and convert it to the specified timezone.
        return datetime.now(pytz.timezone('UTC')).astimezone(pytz.timezone(settings.TIMEZONE))

    def write_to_log(self, content):
        """
        Write content to the log file with a timestamp.

        Args:
            content (str): Content to be written to the log file.
        """
        try:
            log_filepath = settings.LOG_FILE_DIR
            log_file = open(log_filepath, 'a+', encoding='utf8')
            
            # Get the current time with timezone
            current_datetime = self.get_current_time()
            datetime_string = current_datetime.strftime('%Y-%m-%d %H:%M:%S.%f')
            
            log_entry_format = f'{datetime_string} | {content}\n'
            
            # Print the log entry and write it to the log file.
            print(log_entry_format)
            log_file.write(log_entry_format)
            log_file.close()
        except Exception as e:
            current_class = self.__class__.__name__
            # Handle and log any exceptions that occur during file writing.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)

    def load_cookies(self, cookie_filepath):
        """
        Load cookies from a JSON file.

        Args:
            cookie_filepath (str): Path to the cookie JSON file.

        Returns:
            dict: Loaded cookies as a dictionary.
        """
        try:
            cookie_file = open(cookie_filepath, 'rt', encoding='utf-8')
            cookie_content = cookie_file.read()
            
            # Parse the JSON content into a dictionary.
            cookies = json.loads(cookie_content)
            cookie_file.close()
        except Exception as e:
            cookies = {}
            current_class = self.__class__.__name__
            # Handle and log any exceptions that occur during cookie loading.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
        finally:
            # Return the loaded cookies as a dictionary.
            return cookies

    def generate_user_agent(self):
        """
        Generate a User-Agent string using the fake_useragent library.

        Returns:
            str: Generated User-Agent string.
        """
        try:
            user_agent_generator = UserAgent()
            
            # Generate a User-Agent string for Google Chrome.
            user_agent = user_agent_generator['google chrome']
        except Exception as e:
            current_class = self.__class__.__name__
            # Handle and log any exceptions that occur during User-Agent generation.
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            user_agent = str()
        finally:
            # Return the generated User-Agent string.
            return user_agent
